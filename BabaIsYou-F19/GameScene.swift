//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var baba: SKSpriteNode!
    var Thingblock : SKSpriteNode!
    var Resultblock : SKSpriteNode!
    var isblock : SKSpriteNode!
    var Wingame = true
    
    var baba_speed : CGFloat = 20

    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        self.baba = self.childNode(withName: "baba") as! SKSpriteNode
        self.Thingblock = self.childNode(withName: "Thingblock") as! SKSpriteNode
        self.Resultblock = self.childNode(withName: "Resultblock") as! SKSpriteNode
        self.isblock = self.childNode(withName: "isblock") as! SKSpriteNode
        physicsWorld.contactDelegate = self
        
        
        
        self.baba = self.childNode(withName:"baba") as! SKSpriteNode
        let babaSize = CGSize(width: 60, height: 60)
        self.baba.physicsBody =  SKPhysicsBody(rectangleOf: babaSize)
        self.baba.physicsBody!.isDynamic = false

        self.Thingblock = self.childNode(withName:"Thingblock") as! SKSpriteNode
        let thingsize = CGSize(width: 60, height: 60)
        self.Thingblock.physicsBody =  SKPhysicsBody(rectangleOf: thingsize)
        self.Thingblock.physicsBody!.isDynamic = false
        
        self.Resultblock = self.childNode(withName:"Resultblock") as! SKSpriteNode
        let resultsize = CGSize(width: 60, height: 60)
        self.Resultblock.physicsBody =  SKPhysicsBody(rectangleOf: resultsize)
        self.Resultblock.physicsBody!.isDynamic = false
        
        self.isblock = self.childNode(withName:"Resultblock") as! SKSpriteNode
        let issize = CGSize(width: 60, height: 60)
        self.isblock.physicsBody =  SKPhysicsBody(rectangleOf: issize)
        self.isblock.physicsBody!.isDynamic = false
        
        
        
    }
   
    func didBegin(_ contact: SKPhysicsContact) {
        if(self.Wingame == false){
            return
        }
        print("Something collided!")
        let collision = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask

        if collision == PhysicsCategory.ThingBlock & PhysicsCategory.IsBlock | PhysicsCategory.ResultBlock {
            print("Baba can pass the wall ")
            
//            print("CONGRATULATIONS")
        }else if collision == PhysicsCategory.ResultBlock & PhysicsCategory.IsBlock | PhysicsCategory.ThingBlock{
            print("Baba cannot pass the wall")
        }
        
//        func playerWins() {
//            self.gameInProgress = false
//
//            let message = SKLabelNode(text:"CONGRATULATIONS")
//            message.position = CGPoint(x:self.size.width/2, y:self.size.height/2)
//            message.fontColor = UIColor.magenta
//            message.fontSize = 100
//            message.fontName = "AvenirNext-Bold"
//
//            addChild(message)
//
//
//        }
        
            
            
        
        
        struct PhysicsCategory {
            static let None:  UInt32 = 0
            static let Baba:   UInt32 = 0b1      // 0x00000001 = 1
            static let ThingBlock: UInt32 = 0b10     // 0x00000010 = 2
            static let IsBlock:   UInt32 = 0b100    // 0x00000100 = 4
            static let ResultBlock: UInt32 = 0b1000   // 0x00001000 = 8
            
        }
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        let mouseTouched = touches.first
        if (mouseTouched == nil){
            return
        }
        let location = mouseTouched!.location(in: self)
         let nodeTouched = atPoint(location).name
               print("Player touched: \(nodeTouched)")
        if (nodeTouched == "Upbutton") {
            // move up
            self.baba.position.y = self.baba.position.y + baba_speed
        }
        
        if (nodeTouched == "Downbutton") {
            // move up
            self.baba.position.y = self.baba.position.y - baba_speed
        }
        
        if (nodeTouched == "Leftbutton") {
            // move up
            self.baba.position.x = self.baba.position.x - baba_speed
        }
        
        if (nodeTouched == "Rightbutton") {
                   // move up
                   self.baba.position.x = self.baba.position.x + baba_speed
               }
        
        
        
        
        
        
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
}
